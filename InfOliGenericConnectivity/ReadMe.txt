Sample Compiling:

gcc -w -c -O3 -lm infoli_simple.c
gcc infoli_simple.o -o infoli_simple.x -O3 -lm

Usage: ./infoli_simple.x <Net_Size> <Iapp_input_file> or ./InferiorOlive <Net_Size>

Brain Simulation Time is defined in infoli_simple.h in the SIMTIME definition. 
Default value is 1000ms. If you wish to run for different simulation times
edit this value and recompile.  

Notes:

CellConnections is a file that holds an NxN matrix, where N is
the size of the cell Network. The convention is that a neuron with
an ID that equals to the row number sends data to the neuron with an
ID that equals to the column number via a gap junction that features
conductivity equal to the value of this NxN matrix cell. This means
two things:

1) If you want to model a neuron network where neuron A and neuron B
are use one gap junction to communicate and thus, information exchange
A->B and B->A uses a single conductivity value, this NxN matrix needs to
be symmetric.
2) All diagonal elements are equal to zero since there is no such things
as a cell feeding itself with info.

Currently the CellConnections file defines a 1000x1000 matrix (thus a network of
1000 neurons). You can use you any network size <=1000 with this matrix without
issues. For higher sizes the Matrix needs to be edited. There is also a 10 cell 
sample file for quick experiments.  

In case you want to use this model for all-to-all communication, fill all
matrix cells with the non-zero conductivity value(s) that you need, while
complying to notes 1) and 2) or change the ALLTOALL flag on infoli_simple.h to 1.
You can then use any network size with the all-to-all benchmark active. 

IMPORTANT: Use gcc 6.x+
